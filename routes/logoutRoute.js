const express = require('express');
const router = express.Router();
const logoutController = require('../controller/user/logout');

// 로그아웃 라우트
router.get('/', logoutController.logout);

module.exports = router;