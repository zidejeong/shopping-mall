const express = require('express');
const router = express.Router();
const manageController = require('../controller/user/manage');

// 관리자 페이지 라우트
router.use(`/`, manageController.manage);

module.exports = router;