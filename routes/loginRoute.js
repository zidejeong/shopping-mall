const express = require('express');
const router = express.Router();
const loginController = require('../controller/user/login');
const checkController = require('../controller/user/logincheck');

// 로그인 라우트
router.get('/', loginController.login);
router.post('/checkId', checkController.loginChecked);

// 아이디 찾기 라우트
router.get('/findId', loginController.findId);
router.post('/searchName', checkController.searchName);

// 비밀번호 찾기 라우트
router.get('/findPw', loginController.findPw);
router.post('/searchPw', checkController.searchPw);

// 비밀번호 변경 라우트
router.get('/changePw/:name', loginController.changePw);
router.post('/changePassword/', checkController.changePassword);

module.exports = router;