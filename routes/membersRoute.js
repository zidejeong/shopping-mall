const express = require('express');
const router = express.Router();
const membersController = require('../controller/user/members');

// 회원상세 페이지 라우트
router.get('/detail/:idx', membersController.memberDetail);

// 회원상세 수정 페이지 라우트
router.post('/modify/:idx', membersController.memberModify);

// 회원삭제 페이지 라우트
router.post('/del/:idx', membersController.memberDel);

// 회원검색 라우트
router.post('/search', membersController.memberSearch);

// 회원관리 리스트 페이지 라우트
router.use('/:idx', membersController.members);

module.exports = router;