const express = require('express');
const router = express.Router();
const productController = require('../controller/products/category');

// 상품 카테고리 라우트
router.get('/category', productController.categoryRead);
router.post('/category', productController.categorySave);
router.delete('/category', productController.categoryDel);

module.exports = router;