const express = require('express');
const router = express.Router();
const signupController = require('../controller/user/signup');
const checkController = require("../controller/user/logincheck");

// 관리자 페이지 라우트
router.get('/', signupController.signup);
router.post('/save', signupController.signupSave);
router.post('/checkedId', checkController.checkedId);


module.exports = router;