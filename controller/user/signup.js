const signupModel = require("../../models/userModel/signupModel");

const signup = (req, res) => {
    res.render('user/signup', {
        title: '회원가입'
    });
};

const signupSave = (req, res) => {
    const data = signupModel.signupSave(req.body);
    data.then(data => {
        if (data) {
            res.send({ data });
        } else {
            console.error(data);
        }
    });
}

module.exports = {
    signup,
    signupSave,
};