const memberModel = require('../../models/userModel/memberModel');
const sessionModel = require('../../models/userModel/sessionModel');

const members = (req, res) => {
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const userId = sessionModel.readSession(req.cookies.SESSION_ID);
    userId.then(id => {
      let data;
      if (req.query.searchSelected && req.query.searchBox) {
        data = memberModel.memberList(req.params.idx, req.query.searchSelected, req.query.searchBox);
      } else {
        data = memberModel.memberList(req.params.idx);
      }
      data.then(data => {
        res.render('user/members', {title: '회원관리 페이지', members: data.results, paging: data.paging, name: id,
          searchSelected: req.query.searchSelected, searchBox: req.query.searchBox});
      })
    })
  } else {
    res.redirect('/logout');
    return;
  }
};

const memberSearch = (req, res) => {
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const userId = sessionModel.readSession(req.cookies.SESSION_ID);
    userId.then(id => {
      const data = memberModel.memberSearch(req.body);
      data.then(data => {
        console.log("search :::: ", data[0])
        console.log("search ::: ", id)
        /*const results = data.results[0]*/
        /*res.render('user/members', {title: '회원관리 페이지', members: data.results[0], paging: data.paging, name: id});*/
        res.send({title: '회원관리 페이지', members: data[0], paging: data.paging, name: id});
      })
    })
  } else {
    res.redirect('/logout');
    return;
  }
};

const memberDetail = async (req, res) => {
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const userId = await sessionModel.readSession(req.cookies.SESSION_ID);
    const data = memberModel.memberDetail(req.params.idx);
    data.then(data => {
      res.render('user/memberDetail', { title: '상세페이지', memberDetail: data.result, name: userId,
        currentPage: req.query.currentPage });
    });
  } else {
    res.redirect('/logout');
    return;
  }
};

const memberModify = (req, res) => {
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    memberModel.memberModify(req.params.idx, req.body).then(() => {
      res.send({"result": true});
    });
  } else {
    res.redirect('/logout');
    return;
  }
}

const memberDel = (req, res) => {
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const data = memberModel.memberDel(req.params.idx);
    data.then(data => {
      if (data) {
        res.redirect('/members/1');
      } else {
        console.error(data);
      }
    });
  } else {
    res.redirect('/logout');
    return;
  }

}

module.exports = {
  members,
  memberDetail,
  memberModify,
  memberDel,
  memberSearch,
};