const sessionModel = require('../../models/userModel/sessionModel');

const logout = async (req, res) => {
    if (req.cookies.SESSION_ID) {
        const deletedSessionId = sessionModel.delSession(req.cookies.SESSION_ID);
        deletedSessionId.then(data => {
            res.clearCookie('SESSION_ID');
            res.redirect('/login');
        })
    } else {
        res.redirect('/login');
    }
};

module.exports = {
    logout
};