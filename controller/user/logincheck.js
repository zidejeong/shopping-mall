const checkModel = require("../../models/userModel/loginModel");

const checkedId = (req, res) => {
    const data = checkModel.checkedId(req.body);
    data.then(result => {
        // 사용중인 아이디 인지 확인 (true: 사용중인 아이디, false: 미사용 아이디)
        res.send(result);
    });
}

const loginChecked = (req, res) => {
    const data = checkModel.loginChecked(req.body);
    data.then(result => {
        res.cookie('SESSION_ID', result.sessionId, {
            maxAge: 300000,
            httpOnly: true,
            path: '/',
        });
        res.send(result);
    });
}

const searchName = (req, res) => {
    const data = checkModel.searchName(req.body);
    data.then(result => {
        res.send(result);
    });
}

const searchPw = (req, res) => {
    const data = checkModel.searchPassword(req.body);
    data.then(result => {
        res.send(result);
    });
}

const changePassword = (req, res) => {
    const data = checkModel.changePassword(req.body);
    data.then(result => {
        res.send(result);
    });
}

module.exports = {
    checkedId,
    loginChecked,
    searchName,
    searchPw,
    changePassword,
};