
const login = (req, res) => {
    res.render('user/login', {
        title: '로그인'
    });
};

const findId = (req, res) => {
    res.render('user/findId', {
        title: '아이디 찾기'
    })
}

const findPw = (req, res) => {
    res.render('user/findPw', {
        title: '비밀번호 찾기'
    })
}

const changePw = (req, res) => {
    res.render('user/changePw', {
        title: '비밀번호 변경',
        m_id: req.params.name,
    })
}

module.exports = {
    login,
    findId,
    findPw,
    changePw,
};