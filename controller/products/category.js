const categoryModel = require("../../models/productModel/categoryModel");

const categoryRead = (req, res) => {
  console.log("category : ", req.query.m_id);
  console.log("categoryRead : ", req.query);
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const data = categoryModel.categoryRead(req.query.seq);
    data.then(data => {
      /*console.log("category read : >>", data);*/
      if ( req.query.m_id ) {
        res.render('products/category', {title: '상품분류관리 페이지', name: req.query.m_id, category: data});
      } else {
        res.send({category: data})
      }
    })
  } else {
    res.redirect('/logout');
    return;
  }
};

const categorySave = (req, res) => {
  console.log("categorySave : ", req.body);
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const data = categoryModel.categorySave(req.body)
    data.then(data => {
      console.log("userId : ", data);
      const result = categoryModel.categoryRead(data);
      result.then(data => {
        console.log("category save read : ", data)
        /*res.send('products/category', {title: '상품분류관리 페이지', name: req.query.m_id, category: data});*/
        res.send({category: data});
      });
    })
  } else {
    res.redirect('/logout');
    return;
  }
}

const categoryDel = (req, res) => {
  console.log('categoryDel : ', req.body);
  if (req.cookies.SESSION_ID) {
    res.cookie('SESSION_ID', req.cookies.SESSION_ID, {
      maxAge: 300000,
      httpOnly: true,
      path: '/',
    });
    const data = categoryModel.categoryDel(req.body.seq)
    data.then(data => {
      console.log("userDel : ", data.affectedRows);
      res.send({category: data});
    })
  } else {
    res.redirect('/logout');
    return;
  }
}

module.exports = {
  categoryRead,
  categorySave,
  categoryDel,
}