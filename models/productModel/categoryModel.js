const mysql2 = require('mysql2/promise');
let dbPool = mysql2.createPool({
  host: 'localhost',
  user: 'jgh',
  password: '1234',
  database: 'store',
  connectionLimit: 10,
  waitForConnections: true
});

exports.categoryRead = async (data) => {
  /*console.log("있냐? ", data);*/
  const seq = data;
  if ( seq === undefined ) {
    const category = await dbPool.query('SELECT * FROM category');
    return category[0];
  } else {
    /*console.log(">>>>>>>>>>>>    ", seq);*/
    const data = await dbPool.query(`SELECT * FROM category WHERE c_parent = ${seq} OR c_seq = ${seq}`);
    /*console.log("NNNNNNN : ", data[0])*/
    return data[0];
  }
}

exports.categorySave = async (data) => {
  if ( Object.keys(data)[0] === 'firstCategory' ) {
    const result = await dbPool.query(`INSERT INTO category SET c_name=?`, [data.firstCategory]);
    return result[0].insertId;
  } else if ( Object.keys(data)[0] === 'secondCategory' ) {
    const result = await dbPool.query('INSERT INTO category SET c_level=?, c_parent=?, c_name=?', [1, data.seq, data.secondCategory]);
    return result[0].insertId;
  } else {
    const result = await dbPool.query('INSERT INTO category SET c_level=?, c_parent=?, c_name=?', [2, data.seq, data.thirdCategory]);
    return result[0].insertId;
  }
}

exports.categoryDel = async (data) => {
  console.log("deldel : ", data)
  const result = await dbPool.query(`SELECT * FROM category WHERE c_parent = ${data}`);
  console.log("readResult : ", result[0].length);
  if ( result[0].length == 0 ) {
    const result = await dbPool.query(`DELETE FROM category WHERE c_seq = ${data}`);
    console.log("delResult : ", result[0])
    return result[0];
  } else {
    return result[0].length;
  }
}