const mysql2 = require('mysql2/promise');

let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '1234',
    database: 'store',
    connectionLimit: 10,
    waitForConnections: true
});

const saveSession = async (data) => {
    const session = await dbPool.query(`INSERT INTO sessions (m_id, s_date) VALUES (?, ?)`, [data, new Date()]);
    return session[0].insertId;
}

const readSession = async (data) => {
    const session = await dbPool.query(`SELECT * FROM sessions WHERE s_seq = ${Number(data)}`);
    return session[0][0].m_id;
};

const delSession = async (data) => {
    const session = await dbPool.query(`DELETE FROM sessions WHERE s_seq = ${Number(data)}`)
    return session;
}

const categorySession = async (data) => {
  const session = await dbPool.query()
}

module.exports= {
    saveSession,
    readSession,
    delSession,
}