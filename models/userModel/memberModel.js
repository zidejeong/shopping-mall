const mysql2 = require('mysql2/promise');
let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '1234',
    database: 'store',
    connectionLimit: 10,
    waitForConnections: true
});

let paging = {};
paging.listCount = 10;              // 화면에 표시될 게시글 수
paging.startListNum = 0;            // 화면에 표시될 게시글 시작번호
paging.endListNum = 0;              // 화면에 표시될 게시글 끝번호
paging.totalListCount = 0;          // 총 게시글 수
paging.totalPage = 0;               // 총 페이지 수
paging.pageCount = 5;               // 화면에 표시될 페이지 수
paging.currentPage = 0;             // 현재 페이지 번호
paging.startPage = 0;               // 화면에 표시될 시작 페이지번호
paging.endPage = 0;                 // 화면에 표시될 끝 페이지번호

function memberList(idx, searchSelected, searchBox) {
  let selectSql;
  if (searchSelected && searchBox) {
    selectSql = `SELECT * FROM member WHERE ${searchSelected} LIKE '%${searchBox}%' ORDER BY m_seq DESC`;
  } else {
    selectSql = 'SELECT * FROM member ORDER BY m_seq DESC';
  }
    const data = dbPool.query(selectSql).then(results => {
      // 총 페이지 수
      paging.totalPage = Math.ceil(results[0].length / paging.listCount);
      // 현재 표시될 페이지 번호
      paging.currentPage = Number(idx);
      // 화면에 표시될 마지막 페이지 번호
      paging.endPage = Math.ceil(paging.currentPage / paging.pageCount) * paging.pageCount;
      // 화면에 표시될 첫 페이지 번호
      paging.startPage = paging.endPage - (paging.pageCount - 1);
      // 총 페이지 수 보다 화면에 표시할 마지막 페이지 번호보다 크면 총 페이지 수를 넣는다.
      if (paging.endPage >= paging.totalPage) {
          paging.endPage = paging.totalPage
      }
      // 화면에 표시될 마지막 게시글 번호
      paging.endListNum = paging.currentPage * paging.listCount - 1;
      // 화면에 표시될 끝번호와 게시글 수를 비교
      if (paging.endListNum <= results[0].length) {
          // 게시글 수가 화면에 표시될 끝번호보다 클 때 첫번째 게시글 번호
          paging.startListNum = paging.endListNum - paging.listCount + 1;
      } else {
          // 게시글 수가 화면에 표시할 끝번호보다 작을 때 첫번째 게시글 번호
          paging.startListNum = paging.endListNum - paging.listCount + 1;
          // 화면에 표시할 끝번호
          paging.endListNum = results[0].length -1;
      }
      // 화면에 표시될 게시글 저장
      let list = [];
      for (let i = paging.startListNum; i <= paging.endListNum; i++) {
          list.push(results[0][i]);
      }
      return {results: list, paging};
    });
    return data;
}

function memberSearch(data) {
  /*console.log(data.select);
  console.log(data.condition);*/
  const selectSql = `SELECT * FROM member WHERE ${data.select} = "${data.condition}"`;
  const search = dbPool.query(selectSql).then(results => {
      console.log("DB :: ", results[0]);
      return results;
  });
  return search;
}

function memberDetail(idx) {
  const selectSql = `SELECT * FROM member WHERE m_seq = ${Number(idx)}`;
  const data = dbPool.query(selectSql).then(result => {
      return { result: result[0]};
  });
  return data;
}

function memberModify(idx, data) {
  let obj = dbPool.query(`UPDATE member SET m_name=?, m_email=?, m_phone=?, m_buying_count=?, m_buying_amount=?, m_detail_address=?, m_grade=?, m_note=? WHERE m_seq=?`,
      [data.m_name, data.m_email, data.m_phone, data.m_buying_count, data.m_buying_amount, data.m_detail_address, data.m_grade, data.m_note, Number(idx)]);
  console.log("modify : ", obj);
  return obj;
}

function memberDel(idx) {
  const data = dbPool.query(`DELETE FROM member WHERE m_seq = ${Number(idx)}`).then( result => {
      return result;
  });
  return data;
}


module.exports = {
    memberList,
    memberDetail,
    memberModify,
    memberDel,
    memberSearch,
};


