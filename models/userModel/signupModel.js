const mysql2 = require('mysql2/promise');
const bcrypt = require('bcrypt');
let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '1234',
    database: 'store',
    connectionLimit: 10,
    waitForConnections: true
});

async function signupSave(data) {
    const hash = await bcrypt.hash(data.m_password, 12);
    const result = dbPool.query('INSERT INTO member SET m_id=?, m_name=?, m_email=?, m_phone=?, m_password=?, m_signup_date=?, m_last_date=?, m_post=?, m_address=?, m_detail_address=?',
        [data.m_id, data.m_name, data.m_email, data.m_phone, hash, new Date(), new Date(), data.m_post, data.m_address, data.m_detail_address])
        .then(result => {
            return result;
    });
    return result;
}

module.exports = {
    signupSave,
};