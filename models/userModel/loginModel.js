const mysql2 = require('mysql2/promise');
const bcrypt = require('bcrypt');
const sessionModel = require('./sessionModel');

let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '1234',
    database: 'store',
    connectionLimit: 10,
    waitForConnections: true
});

function checkedId(data) {
    const result = dbPool.query(`SELECT * FROM member WHERE m_id = "${data.m_id}"`)
        .then(result => {
            try {
                if (result[0] == "") {
                    return false;   // 일치하는 아이디가 없을 때
                } else {
                    return true;    // 일치하는 아이디가 있을 때
                }
            } catch (err) {
                console.error(err);
            }
        });
    return result;
}

async function loginChecked(data) {
    const result = await dbPool.query(`SELECT * FROM member WHERE m_id = "${data.m_id}"`)
        .then(async (result) => {
            try {
                if (result[0] == false) {
                    return {m_id: false, m_grade: '아이디가 없습니다!'};
                } else {
                    const compare = await bcrypt.compare(data.m_password, result[0][0].m_password);
                    if (compare) {  // 일치하는 아이디가와 비밀번호가 맞을 때
                        const sessionId = await sessionModel.saveSession(data.m_id)
                        return {m_id: true, m_grade: result[0][0].m_grade, sessionId, m_name: result[0][0].m_name};
                    } else {    // 아이디는 일지하지만 비밀번호가 다를 때
                        return {m_id: false, m_grade: '비밀번호가 다릅니다!'};
                    }
                }
            } catch (err) {
                console.error(err);
            }
        });
    return result;
}

async function searchName(data) {
    const result = await dbPool.query(`SELECT * FROM member WHERE m_name = "${data.m_name}" and m_email = "${data.m_email}"`)
        .then(result => {
            try {
                if (result[0] == false) {
                    return {m_id: false, m_grade: '사용자가 없습니다.'};
                } else {
                    return {m_id: true, m_grade: result[0][0].m_id};
                }
            } catch (err) {
                console.error(err);
            }
        });
    return result;
}

async function searchPassword(data) {
    const result = await dbPool.query(`SELECT * FROM member WHERE m_id = "${data.m_id}" and m_phone = "${data.m_phone}"`)
        .then(result => {
            try {
                if (result[0] == false) {
                    return {m_id: false, m_grade: '사용자가 없습니다.'};
                } else {
                    return {m_id: true, m_grade: '비밀번호를 변경하시겠습니까?'};
                }
            } catch (err) {
                console.error(err);
            }
        });
    return result;
}

async function changePassword(data) {
    const hash = await bcrypt.hash(data.m_password, 12);
    console.log("hash : ", hash, data.m_id)
    const result = await dbPool.query(`UPDATE member SET m_password=? WHERE m_id=?`,[hash, data.m_id])
        .then(result => {
            console.log(";;; LLL : ", result)
            try {
                return result;
            } catch (err) {
                console.error(err);
            }
        });
    return result;
}

module.exports = {
    checkedId,
    loginChecked,
    searchName,
    searchPassword,
    changePassword,
};