const express = require('express');
const server = express();
const cookieParser = require('cookie-parser');
/*const session = require('express-session');*/
const path = require('path');
const morgan = require('morgan');
const helmet = require('helmet');
const dotenv = require('dotenv');
const loginRoute = require('./routes/loginRoute');
const logoutRoute = require('./routes/logoutRoute');
const manageRoute = require('./routes/manageRoute');
const membersRoute = require('./routes/membersRoute');
const productRoute = require('./routes/productRoute');
const signupRoute = require('./routes/signupRoute');

dotenv.config();
const port = process.env.PORT;

server.set('view engine', 'ejs');
server.set('views', path.join(__dirname, '/templates'));

server.use(morgan('dev'));
server.use(helmet({contentSecurityPolicy: false}))
server.use(cookieParser());

server.use(express.static(path.join(__dirname, '/statics')));
server.use(express.urlencoded({ extended: true }));
server.use(express.json());

// 로그인
server.use('/login', loginRoute);

// 로그아웃
server.use('/logout', logoutRoute);

// 관리자 페이지로 이동
server.use('/manage', manageRoute);

// 회원관리 페이지로 이동
server.use('/members', membersRoute);

// 상품 카테고리 페이지로 이동
server.use('/products', productRoute);

// 회원가입 페이지로 이동
server.use('/signup', signupRoute);

// 아이디 및 비밀번호 찾기 페이지로 이동
server.use('/user', loginRoute);

server.use((req, res, nex) => {
    res.status(404).send('Not Found : 찾는 페이지가 없습니다!');
});
server.listen(port, () => {
    console.log(`서버의 ${port}번 포트가 준비되었습니다!`);
});
